package com.example.demo.dto;

public class MovieDto {

    private int movieId;
    private String title;
    private int year;
    private String photo;

    public MovieDto(int movieId, String title, int year, String photo) {
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.photo = photo;
    }

    public int getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getPhoto() {
        return photo;
    }
}
