package com.example.demo.dto;


import java.util.List;
import java.util.stream.Collectors;

public class MovieListDto {
    private List<MovieDto> movies;

    public MovieListDto(List<MovieDto> movies) {
        this.movies = movies;
    }

    @Override
    public String toString() {
        return "{ \"movies\": [" + movies.stream()
                .map(movie ->
                        "{ \n " +
                                "\"movieId\": " + movie.getMovieId() + ",\n"
                                + "\"title\": \"" + movie.getTitle() + "\",\n"
                                + "\"year\": " + movie.getYear() + ",\n"
                                + "\"image\": \"" + movie.getPhoto() + "\""
                                + "}"
                )
                .collect(Collectors.joining(",")) + "] }";
    }

    public List<MovieDto> getMovies() {
        return movies;
    }
}
