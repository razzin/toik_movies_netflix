package com.example.demo.rest;

import com.example.demo.dto.MovieListDto;
import com.example.demo.service.MovieService;
import com.example.demo.service.MovieServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);
    @Autowired
    private MovieService movieService;

    @CrossOrigin
    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> movies() {
        LOGGER.info("---- getting movies " + movieService.getMovies());
        return ResponseEntity.ok().body(movieService.getMovies());
    }
}
