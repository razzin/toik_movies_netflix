package com.example.demo.repository;

import com.example.demo.dto.MovieListDto;

public interface MovieRepository {
    MovieListDto getMovies();
}