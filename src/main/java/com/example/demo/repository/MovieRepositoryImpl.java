package com.example.demo.repository;

import com.example.demo.dto.MovieDto;
import com.example.demo.dto.MovieListDto;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieRepositoryImpl implements MovieRepository {

    private List<MovieDto> movies = new ArrayList<>();

    public MovieRepositoryImpl() {
        movies.add(new MovieDto(1,"Piraci z krzemowej doliny", 1999, "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg"));
        movies.add(new MovieDto(2,"Ja, robot", 2004, "https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg"));
        movies.add(new MovieDto(3,"Kod nieśmiertelności", 2011, "https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg"));
        movies.add(new MovieDto(4,"Ex Machina", 2015, "https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg"));
    }

    public MovieListDto getMovies() {
        return new MovieListDto(this.movies);
    }
}
