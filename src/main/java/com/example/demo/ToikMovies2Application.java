package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToikMovies2Application {

    public static void main(String[] args) {
        SpringApplication.run(ToikMovies2Application.class, args);
    }

}
