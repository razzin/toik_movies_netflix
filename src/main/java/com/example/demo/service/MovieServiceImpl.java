package com.example.demo.service;

import com.example.demo.dto.MovieListDto;
import com.example.demo.repository.MovieRepository;
import com.example.demo.repository.MovieRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieRepository movieRepository;

    public MovieListDto getMovies(){
        return movieRepository.getMovies();
    }
}
