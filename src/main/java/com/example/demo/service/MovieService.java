package com.example.demo.service;

import com.example.demo.dto.MovieListDto;

public interface MovieService {
    MovieListDto getMovies();
}
